package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IRepository;
import ru.tsc.panteleev.tm.model.AbstractModel;
import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    void set(@NotNull Collection<M> models);

    @Nullable
    List<M> findAll();

}
