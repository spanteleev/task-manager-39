package ru.tsc.panteleev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
import ru.tsc.panteleev.tm.model.Project;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project>{

    @Insert("INSERT INTO TM_PROJECT (row_id, name, description, status, created_dt, begin_dt, end_dt, user_id) "
            + "VALUES (#{id}, #{name}, #{description}, #{status}, #{created}, #{dateBegin}, #{dateEnd}, #{userId})")
    void add(@NotNull Project project);

    @Update("UPDATE TM_PROJECT SET name = #{name}, description = #{description}, status = #{status} WHERE row_id = #{id}")
    void update(@NotNull Project project);

    @Insert("INSERT INTO TM_PROJECT (row_id, name, description, status, created_dt, begin_dt, end_dt, user_id) "
            + "VALUES (#{id}, #{name}, #{description}, #{status}, #{created}, #{dateBegin}, #{dateEnd}, #{userId})")
    void set(@NotNull Collection<Project> projects);

    @NotNull
    @Select("SELECT * FROM TM_PROJECT WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "begin_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Project> findAllByUserId(@Param("userId") @NotNull String userId);

    @NotNull
    @SelectProvider(type = SqlProviderAdapter.class, method = "select")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "begin_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Project> findAllSort(@NotNull SelectStatementProvider selectStatementProvider);

    @NotNull
    @Select("SELECT * FROM TM_PROJECT")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "begin_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    List<Project> findAll();

    @Nullable
    @Select("SELECT * FROM TM_PROJECT WHERE user_id = #{userId} AND row_id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "created", column = "created_dt"),
            @Result(property = "dateBegin", column = "begin_dt"),
            @Result(property = "dateEnd", column = "end_dt")
    })
    Project findById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM TM_PROJECT WHERE row_id = #{id} AND user_id = #{userId}")
    void removeById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Delete("DELETE FROM TM_PROJECT WHERE user_id = #{userId}")
    void clearByUserId(@Param("userId") @NotNull String userId);

    @Delete("TRUNCATE TABLE tm_project")
    void clear();

    @Select("SELECT COUNT(1) FROM TM_PROJECT WHERE user_id = #{userId}")
    long getSize(@Param("userId") @NotNull String userId);

    @Select("SELECT COUNT(1) = 1 FROM TM_PROJECT WHERE user_id = #{userId} AND row_id = #{id}")
    boolean existsById(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

}
