package ru.tsc.panteleev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.service.IServiceLocator;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.exception.user.AccessDeniedException;
import ru.tsc.panteleev.tm.model.Session;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    protected Session check(AbstractUserRequest request, Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @Nullable final Session session = serviceLocator.getAuthService().validateToken(token);
        @Nullable final Role roleUser = session.getRole();
        final boolean check = roleUser == role;
        if (!check) throw new AccessDeniedException();
        return session;
    }

    protected Session check(AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

    @Getter
    @Nullable
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
