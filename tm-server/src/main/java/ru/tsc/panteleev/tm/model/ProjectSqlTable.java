package ru.tsc.panteleev.tm.model;

import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;
import ru.tsc.panteleev.tm.enumerated.Status;
import java.sql.JDBCType;
import java.util.Date;

public class ProjectSqlTable extends SqlTable {

    public final SqlColumn<String> id = column("row_id", JDBCType.VARCHAR);

    public final SqlColumn<String> userId = column("user_id", JDBCType.VARCHAR);

    public final SqlColumn<String> name = column("name", JDBCType.VARCHAR);

    public final SqlColumn<String> description = column("description", JDBCType.VARCHAR);

    public final SqlColumn<Status> status = column("status", JDBCType.VARCHAR);

    public final SqlColumn<Date> created = column("created_dt", JDBCType.TIMESTAMP);

    public final SqlColumn<Date> dateBegin = column("begin_dt", JDBCType.TIMESTAMP);

    public final SqlColumn<Date> dateEnd = column("end_dt", JDBCType.TIMESTAMP);

    public ProjectSqlTable() {
        super("tm_project");
    }

}
