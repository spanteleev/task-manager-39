package ru.tsc.panteleev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Insert("INSERT INTO TM_USER (row_id, login, password_hash, email, first_name, last_name, middle_name, locked, role) "
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName},  #{locked}, #{role})")
    void add(@NotNull User user);

    @Update("UPDATE TM_USER SET login = #{login}, password_hash = #{passwordHash}, email = #{email}, first_name = #{firstName}," +
            "last_name = #{lastName}, middle_name = #{middleName}, locked = #{locked}, role = #{role} WHERE row_id = #{id}")
    void update(@NotNull final User user);

    @Insert("INSERT INTO TM_USER (row_id, login, password_hash, email, first_name, last_name, middle_name, locked, role) "
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName},  #{locked}, #{role})")
    void set(@NotNull Collection<User> users);

    @NotNull
    @Select("SELECT row_id, login, password_hash, email, first_name, last_name, middle_name, locked, role FROM TM_USER")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    List<User> findAll();

    @Nullable
    @Select("SELECT row_id, login, password_hash, email, first_name, last_name, middle_name, locked, role " +
            "FROM TM_USER WHERE row_id = #{id}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    User findById(@Param("id") @NotNull String id);

    @Nullable
    @Delete("DELETE FROM TM_USER WHERE row_id = #{id}")
    void remove(@NotNull User model);

    @Nullable
    @Delete("DELETE FROM TM_USER WHERE row_id = #{id}")
    void removeById(@NotNull String id);

    @Delete("TRUNCATE TABLE TM_USER")
    void clear();

    @Select("SELECT count(1) FROM TM_USER WHERE row_id = #{id}")
    long getSize();

    @Nullable
    @Select("SELECT row_id, login, password_hash, email, first_name, last_name, middle_name, locked, role " +
            "FROM TM_USER WHERE login = #{login}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    User findByLogin(@Param("login") @NotNull String login);

    @Nullable
    @Select("SELECT row_id, login, password_hash, email, first_name, last_name, middle_name, locked, role " +
            "FROM TM_USER WHERE email = #{email}")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    User findByEmail(@Param("email") @NotNull String email);

}
